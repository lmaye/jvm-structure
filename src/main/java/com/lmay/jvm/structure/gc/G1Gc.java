package com.lmay.jvm.structure.gc;

/**
 * -- G1 收集器
 * : 标记整理算法实现，运作流程主要包括以下：初始标记，并发标记，最终标记，筛选标记。不会产生空间碎片，可以精确地控制停顿。
 *
 * <pre>
 * VM参数示例: G1
 *     -Xms20M
 *     -Xmx20M
 *     -Xss128K
 *     -Xmn8M
 *     -XX:+PrintGC
 *     -XX:+PrintGCDetails
 *     -XX:+PrintGCDateStamps
 *     -XX:+UseG1GC
 * </pre>
 *
 * @author lmay.Zhou
 * @date 2020/11/7 11:50 星期六
 * @qq 379839355
 * @email lmay@lmaye.com
 * @since JDK1.8
 */
public class G1Gc {
    private static final int MB = 1024 * 1024;

    public static void main(String[] args) {
        byte[] b1 = new byte[4 * MB];
        byte[] b2 = new byte[4 * MB];
        byte[] b3 = new byte[4 * MB];
        byte[] b4 = new byte[2 * MB];
    }
}
