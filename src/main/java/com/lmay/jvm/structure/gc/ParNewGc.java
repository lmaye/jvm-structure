package com.lmay.jvm.structure.gc;

/**
 * -- ParNew 收集器
 * : Serial收集器的多线程版本，也需要Stop The World，复制算法。
 *
 * <pre>
 * VM参数示例: ParNew + CMS
 *     -Xms20M
 *     -Xmx20M
 *     -Xss128K
 *     -Xmn8M
 *     -XX:+PrintGC
 *     -XX:+PrintGCDetails
 *     -XX:+PrintGCTimeStamps
 *     -XX:+PrintTenuringDistribution
 *     -XX:+UseParNewGC
 *     -XX:+UseConcMarkSweepGC
 *
 * ParNew + Serial Old
 * - VM warning: Using the ParNew young collector with the Serial old collector is deprecated and will likely be removed in a future release
 *     -Xms20M
 *     -Xmx20M
 *     -Xss128K
 *     -Xmn8M
 *     -XX:+PrintGC
 *     -XX:+PrintGCDetails
 *     -XX:+PrintGCTimeStamps
 *     -XX:+PrintTenuringDistribution
 *     -XX:+UseParNewGC
 * </pre>
 *
 * @author lmay.Zhou
 * @date 2020/11/6 22:47 星期五
 * @qq 379839355
 * @email lmay@lmaye.com
 * @since JDK1.8
 */
public class ParNewGc {
    private static final int MB = 1024 * 1024;

    public static void main(String[] args) {
        byte[] b1 = new byte[4 * MB];
        byte[] b2 = new byte[4 * MB];
        byte[] b3 = new byte[4 * MB];
        byte[] b4 = new byte[6 * MB];
    }
}
