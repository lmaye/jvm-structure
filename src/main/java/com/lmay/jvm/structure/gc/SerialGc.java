package com.lmay.jvm.structure.gc;

/**
 * -- Serial 收集器
 * : 单线程的收集器，收集垃圾时，必须Stop The World，使用复制算法。
 *
 * <pre>
 * VM参数示例: Serial + Serial Old
 *     -Xms20M
 *     -Xmx20M
 *     -Xss128K
 *     -Xmn8M
 *     -XX:+PrintGC
 *     -XX:+PrintGCDetails
 *     -XX:+PrintGCTimeStamps
 *     -XX:+UseSerialGC
 * </pre>
 *
 * @author lmay.Zhou
 * @date 2020/11/5 22:08 星期四
 * @qq 379839355
 * @email lmay@lmaye.com
 * @since JDK1.8
 */
public class SerialGc {
    private static final int MB = 1024 * 1024;

    public static void main(String[] args) {
        byte[] b1 = new byte[4 * MB];
        byte[] b2 = new byte[4 * MB];
        byte[] b3 = new byte[4 * MB];
        byte[] b4 = new byte[6 * MB];
    }
}
