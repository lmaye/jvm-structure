package com.lmay.jvm.structure.gc;

/**
 * -- 垃圾回收示例 1
 * <pre>
 * VM参数示例:
 *     -Xms20M
 *     -Xmx20M
 *     -Xss128K
 *     -Xmn8M
 *     -XX:+PrintGC
 *     -XX:+PrintGCDetails
 *     -XX:+PrintGCDateStamps
 *     -XX:+PrintGCTimeStamps
 * </pre>
 *
 * @author lmay.Zhou
 * @date 2020/11/5 22:08 星期四
 * @qq 379839355
 * @email lmay@lmaye.com
 * @since JDK1.8
 */
public class Gc01 {
    /**
     * 1MB
     */
    private static final int MB = 1024 * 1024;

    public static void main(String[] args) {
        byte[] b1 = new byte[4 * MB];
        byte[] b2 = new byte[4 * MB];
        byte[] b3 = new byte[4 * MB];
        byte[] b4 = new byte[2 * MB];
    }
}
