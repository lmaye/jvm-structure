package com.lmay.jvm.structure.gc;

/**
 * -- Parallel Scavenge 收集器
 * : 新生代收集器，复制算法的收集器，并发的多线程收集器，目标是达到一个可控的吞吐量。如果虚拟机总共运行100分钟，其中垃圾花掉1分钟，吞吐量就是99%。
 *
 * <pre>
 * Parallel Old 收集器: 是ParallelScavenge收集器的老年代版本，使用多线程，标记-整理算法。
 * VM参数示例: Parallel Scavenge + Parallel Old
 *     -Xms20M
 *     -Xmx20M
 *     -Xss128K
 *     -Xmn8M
 *     -XX:+PrintGC
 *     -XX:+PrintGCDetails
 *     -XX:+UseParallelGC
 * </pre>
 *
 * @author lmay.Zhou
 * @date 2020/11/7 11:48 星期六
 * @qq 379839355
 * @email lmay@lmaye.com
 * @since JDK1.8
 */
public class ParallelScavengeGc {
    private static final int MB = 1024 * 1024;

    public static void main(String[] args) {
        byte[] b1 = new byte[4 * MB];
        byte[] b2 = new byte[4 * MB];
        byte[] b3 = new byte[4 * MB];
        byte[] b4 = new byte[2 * MB];
    }
}
