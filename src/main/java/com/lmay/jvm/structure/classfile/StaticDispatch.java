package com.lmay.jvm.structure.classfile;

/**
 * -- 静态分派
 * <pre>
 *     发生阶段: 编译期
 * </pre>
 *
 * @author lmay.Zhou
 * @date 2020/11/8 22:52 星期日
 * @qq 379839355
 * @email lmay@lmaye.com
 * @since JDK1.8
 */
public class StaticDispatch {
    static abstract class Human {

    }

    static class Man extends Human {

    }

    static class Woman extends Human {

    }

    public void sayHello(Human human) {
        System.out.println("Human say: Hello!");
    }

    public void sayHello(Man man) {
        System.out.println("Man say: Hello!");
    }

    public void sayHello(Woman woman) {
        System.out.println("Woman say: Hello!");
    }

    public static void main(String[] args) {
        Human man = new Man();
        Human woman = new Woman();

        StaticDispatch sd = new StaticDispatch();
        sd.sayHello(man);
        sd.sayHello(woman);
    }
}
