package com.lmay.jvm.structure.classfile;

import java.io.Serializable;

/**
 * -- 继承线程类
 * :: 仅用于结构解析
 *
 * @author lmay.Zhou
 * @date 2019/5/18 9:33 星期六
 * @qq 379839355
 * @email lmay@lmaye.com
 */
public class TestThread extends Thread implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 求和
     *
     * @param y y
     * @return sum
     */
    public int sum(int y) {
        int x = 5;
        return x + y;
    }

    /**
     * 空方法
     */
    public void test() {
    }
}
