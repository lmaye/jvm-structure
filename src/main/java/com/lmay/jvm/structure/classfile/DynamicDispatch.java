package com.lmay.jvm.structure.classfile;

/**
 * -- 动态分派
 * <pre>
 *     发生阶段: 运行期
 * </pre>
 *
 * @author lmay.Zhou
 * @date 2020/11/8 22:53 星期日
 * @qq 379839355
 * @email lmay@lmaye.com
 * @since JDK1.8
 */
public class DynamicDispatch {
    static abstract class Human {
        /**
         * Say Hello
         */
        abstract void sayHello();
    }

    static class Man extends Human {
        @Override
        void sayHello() {
            System.out.println("Man say : Hello!");
        }
    }

    static class Woman extends Human {
        @Override
        void sayHello() {
            System.out.println("Woman say : Hello!");
        }
    }

    public static void main(String[] args) {
        Human man = new Man();
        Human woman = new Woman();
        man.sayHello();
        woman.sayHello();
    }
}
